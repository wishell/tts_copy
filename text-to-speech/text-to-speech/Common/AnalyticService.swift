//
//  AnalyticService.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation
import FirebaseAnalytics

enum AnalyticEvent: String {
    case settings_action
}

class AnalyticService {
    
    func track(_ event: AnalyticEvent, args: [String: String]? = nil) {
        Analytics.logEvent(event.rawValue, parameters: args)
    }
    
}
