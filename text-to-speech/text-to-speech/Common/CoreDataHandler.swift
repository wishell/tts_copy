//
//  CoreDataHandler.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 6/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import CoreData


final class CoreDataHandler {

//    lazy var appDelegate = { UIApplication.shared.delegate as! AppDelegate }()
    var appDelegate: AppDelegate
    lazy var context = { appDelegate.persistentContainer.viewContext }()
    
    init (delegate: AppDelegate) {
        appDelegate = delegate
    }

    func fetch<T: CoreDataType>() -> [T]? {
        let fetchRequest: NSFetchRequest<T.DataType> = T.fetchRequest()
        var files: [T] = []
        do {
            files = try context.fetch(fetchRequest) as! [T]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return files
    }
    
    func fetch <T: CoreDataType>() -> T? {
        let fetchRequest: NSFetchRequest<T.DataType> = T.fetchRequest()
        do {
            let item  = try context.fetch(fetchRequest) as! T
            return item
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return nil
    }

    func save(note: Note) -> TtsItem? {
        guard let entity = NSEntityDescription.entity(forEntityName: String(describing: "TtsItem"),
                                                      in: context) else { return nil }
        let object = TtsItem(entity: entity, insertInto: context)
        object.id = note.id
        object.name = note.name
        object.text = note.content
        object.creationDate = note.date as NSDate
        do {
            try context.save()
            return object
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        return nil
    }
    
    func patch(note: Note) {
        let fetchRequest: NSFetchRequest<TtsItem.DataType> = TtsItem.fetchRequest()
        fetchRequest.predicate = NSPredicate.init(format: "id==%@", note.id as CVarArg)
        fetchRequest.fetchLimit = 1
        let objects = try! context.fetch(fetchRequest)
        guard let obj = objects.first else { return }
         obj.name = note.name
         obj.text = note.content
         obj.creationDate = note.date as NSDate
        do {
            try context.save()
        } catch {
            print("Could not patch. \(error)")
        }
    }
    
    func delete(name: String) {
        let fetchRequest: NSFetchRequest<TtsItem> = TtsItem.fetchRequest()
        fetchRequest.predicate = NSPredicate.init(format: "name==%@", name)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save()
        } catch {
            print("Could not delete. \(error)")
        }
    }
    
    func delete<T: CoreDataType & NSManagedObject>(object: T) {
        context.delete(object)
        do {
            try context.save()
        } catch {
            print("Could not delete. \(error)")
        }
    }

}
