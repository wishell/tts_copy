//
//  TtsItem+CoreDataProperties.swift
//  
//
//  Created by Anton Vishnevsky on 9/10/19.
//
//

import Foundation
import CoreData

protocol CoreDataType {
    associatedtype DataType: NSFetchRequestResult
    static func fetchRequest() -> NSFetchRequest<DataType>
}

extension TtsItem: CoreDataType {
    typealias DataType = TtsItem
    
    static func fetchRequest() -> NSFetchRequest<DataType> {
        return NSFetchRequest<DataType>(entityName: "TtsItem")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var text: String?
    @NSManaged public var creationDate: NSDate?

}
