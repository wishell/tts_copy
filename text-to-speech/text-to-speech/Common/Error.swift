//
//  Error.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 6/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

enum TtsError: Error {
    case undefined_error
}
