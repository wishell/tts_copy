//
//  MainTabBar.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/27/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

protocol TabBarMiddleButtonProtocol: class {
    var  buttonAction: EmptyClosure? { get set }
    var  middleButton: HighlightButton! { get set }
    func hideButton()
    func setupMiddleButton()
    func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
    func handle(index: Int?)
}

class MainTabBar: UITabBar, TabBarMiddleButtonProtocol {
    
    var middleButton: HighlightButton!
    var buttonAction: EmptyClosure?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupMiddleButton()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isHidden {
            return super.hitTest(point, with: event)
        }
        
        let from = point
        let to = middleButton.center
        
        return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)) <= 39 ? middleButton : super.hitTest(point, with: event)
    }
    func hideButton() {
        middleButton.isHidden = true
    }
    
    func setupMiddleButton() {
        middleButton = middleButton ?? HighlightButton()
        middleButton.isHidden = false
        middleButton.frame.size = CGSize(width: 70, height: 70)
        var middleButtonFrame = middleButton.frame
        middleButtonFrame.origin.y = (bounds.height - middleButtonFrame.height) - 2
        middleButtonFrame.origin.x = bounds.width/2 - middleButtonFrame.size.width/2
        middleButton.frame = middleButtonFrame
        
        
        middleButton.setBackgroundImage(#imageLiteral(resourceName: "addNew"), for: .normal)
        middleButton.layer.cornerRadius = 35
        middleButton.layer.masksToBounds = true
        middleButton.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: 0)
        middleButton.addTarget(self, action: #selector(buttonDidPressed), for: .touchUpInside)
        addSubview(middleButton)
    }
    
    @objc
    func buttonDidPressed() {
        print("buttonAction")
        buttonAction?()
    }
    
    func handle (index: Int?) {
        index.flatMap { $0 == 0 ? setupMiddleButton() : hideButton() }
    }
    
}


