//
//  MainTabBarViewController.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class MainTabBarViewController: UITabBarController {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index: Int? = tabBar.items?.firstIndex(of: item)
        (tabBar as! TabBarMiddleButtonProtocol).handle(index: index)
    }

}


