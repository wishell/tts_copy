//
//  Result.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 6/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}

extension Result {
    
    static var empty: Result<Void> { return .success(()) }
    
    static func empty(_ error: Error?) -> Result<Void> {
        if let `error` = error {
            return .failure(error)
        } else {
            return empty
        }
    }
    
    static func error<T>(_ error: Error?) -> Result<T> {
        return .failure(error ?? TtsError.undefined_error)
    }
    
    var successed: Bool {
        if case .success = self {
            return true
        } else {
            return false
        }
    }
    
}
