//
//  ServiceLocator.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/16/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

protocol ServiceLocator {
    func getService<T>(_: T.Type) -> T
    func getService<T>() -> T
}

extension ServiceLocator {

    func getService<T>() -> T {
        return getService(T.self)
    }

}

final class LazyServiceLocator: ServiceLocator {
    
    func typeName(_ some: Any) -> String {
        return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
    
    fileprivate lazy var registry: [String: Any] = [:]
        
    func addService<T>(_ instance: T) {
        let key = typeName(T.self)
        registry[key] = instance
    }
    
    func getService<T>(_: T.Type) -> T {
        return registry[typeName(T.self)] as! T
    }
    
}
