//
//  StoryboardSegue.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/26/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

struct StoryboardSegue: SegueType {
    var id: String
}
