//
//  Style.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 9/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

class Style {
    
    func apply() {
        let tintColor: UIColor = .white
        let barTintColor = UIColor(patternImage: #imageLiteral(resourceName: "background").resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), resizingMode: .stretch))
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor: tintColor]
        NavigationBar.appearance().titleTextAttributes = attributes
        NavigationBar.appearance().largeTitleTextAttributes = attributes
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = barTintColor
            appearance.titleTextAttributes = attributes
            appearance.largeTitleTextAttributes = attributes

            NavigationBar.appearance().tintColor = tintColor
            NavigationBar.appearance().standardAppearance = appearance
            NavigationBar.appearance().compactAppearance = appearance
            NavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            NavigationBar.appearance().tintColor = tintColor
            NavigationBar.appearance().barTintColor = barTintColor
            NavigationBar.appearance().isTranslucent = false
        }
    }
    
}
