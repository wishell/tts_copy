//
//  BundleExtension.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

extension Bundle {
    
    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
    
    var bundleName: String {
        return infoDictionary?["CFBundleName"] as? String ?? ""
    }
    
}
