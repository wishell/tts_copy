//
//  Date.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/10/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

extension Date {
    
    static var now: Date { return Date() }
    
}
