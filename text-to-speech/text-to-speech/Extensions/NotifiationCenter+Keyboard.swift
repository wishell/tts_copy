//
//  NotifiationCenter+Keyboard.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/29/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

extension NotificationCenter {
    
    func observeKeyboard(_ using: @escaping (CGRect) -> Void) {
        let completion: (Notification) -> Void = {
            notification in
            guard let userInfo = notification.userInfo,
                let rect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
            using(rect)
        }
        addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: completion)
        addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main, using: completion)
    }
    
}
