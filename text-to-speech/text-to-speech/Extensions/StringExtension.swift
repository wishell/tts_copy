//
//  StringExtension.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

extension String {
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
}
