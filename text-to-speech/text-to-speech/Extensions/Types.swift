//
//  Types.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 6/10/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

typealias EmptyClosure = () -> ()
typealias StringClosure = ((String) -> ())
typealias NoteClosure = ((Note) -> ())

