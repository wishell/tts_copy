//
//  UIStoryboardSegue+Equitable.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/26/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

extension UIStoryboardSegue {
    
    func equal(_ rhs: StoryboardSegue) -> Bool {
        return identifier == rhs.id
    }
    
}
