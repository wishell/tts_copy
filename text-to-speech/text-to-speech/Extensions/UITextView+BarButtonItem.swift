//
//  UITextView+BarButtonItem.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/29/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import Foundation

struct TextViewAssociatedKeys {
    static var doneAction = "\(Bundle.identifier).doneAction"
}

class ActionWrapper<T>: NSObject {

    let action: T?

    init(_ action: T?) {
        self.action = action
    }

}

extension UITextView {
    
    typealias ActionType = EmptyClosure
    
    var doneAction: ActionType? {
        get {
            guard let action =  objc_getAssociatedObject(self,&TextViewAssociatedKeys.doneAction) as? ActionWrapper<ActionType> else { return nil}
            return action.action
        }
        set {
            objc_setAssociatedObject(self, &TextViewAssociatedKeys.doneAction, ActionWrapper<ActionType>(newValue), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
}

extension UITextView {
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let toolBar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolBar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [flexSpace, done]
        toolBar.items = items
        toolBar.sizeToFit()
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
        doneAction?()
    }

}
