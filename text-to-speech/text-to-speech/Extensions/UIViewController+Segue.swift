//
//  UIViewController+Segue.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/26/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func perform(segue: StoryboardSegue, with sender: Any? = nil) {
        performSegue(withIdentifier: segue.id, sender: sender)
    }
    
}
