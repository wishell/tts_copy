//
//  Note.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/10/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

final class Note {
    
    var id: UUID
    var name: String
    var content: String
    var date: Date
    var textDate: String {
        let shortDateFormatter = DateFormatter()
        shortDateFormatter.dateFormat = "dd.MM.yyyy"
        return shortDateFormatter.string(from: date)
    }
    
    init(name: String = "", content: String = "", date: Date = .now) {
        self.id = UUID()
        self.name = name
        self.content = content
        self.date = date
    }
    
    init(file: TtsItem) {
        id = file.id ?? UUID()
        name = file.name ?? ""
        content = file.text ?? ""
        date = file.creationDate as Date? ?? .now
    }
    
}
