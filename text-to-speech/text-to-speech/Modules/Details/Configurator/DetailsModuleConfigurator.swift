//  
//  DetailsModuleConfigurator.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

final class DetailsModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? DetailsViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: DetailsViewController) {
        let model = DetailsModel()
        model.output = viewController
        model.coreDataHandler = serviceLocator.getService(CoreDataHandler.self)
        
        viewController.model = model
    }
    
}
