//  
//  DetailsModuleInitializer.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

final class DetailsModuleInitializer: NSObject {
    
    @IBOutlet weak var viewController: DetailsViewController!
    
    override func awakeFromNib() {
        let configurator = DetailsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: viewController)
    }
    
}
