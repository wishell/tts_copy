//  
//  DetailsViewController.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import AVFoundation

final class DetailsViewController: UIViewController {
    
    lazy var contentView: DetailsViewInput = { return view as! DetailsViewInput }()
    var model: DetailsModelInput!
    let center = NotificationCenter.default
    var color: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideRightNavButton()
        contentView.saveText  = { [unowned self] text in
            self.model.text = text
            self.hideRightNavButton()
        }
        navigationController?.delegate = self
        
        center.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: {
            [weak self] _ in
            self?.showRightNavButton()
        })
        
        contentView.display(model.note?.content)
    }
    
    @objc
    private func hideRightNavButton() {
        if let button = navigationItem.rightBarButtonItem {
                self.color = button.tintColor
            UIView.animate(withDuration: 0.9) {
                button.tintColor = .clear
            }
        }
    }
    
    @objc
    private func showRightNavButton() {
        if let button = navigationItem.rightBarButtonItem {
            button.isEnabled = true
            button.tintColor = self.color
            UIView.animate(withDuration: 0.9) {
                button.tintColor = self.color
            }
        }
    }
    
}

// MARK: - DetailsModelOutput
extension DetailsViewController: DetailsModelOutput {}

// MARK: - DetailsViewControllerInput
extension DetailsViewController: DetailsViewControllerInput {

    func set(_ note: Note) {
        model.note = note
        model.isNewItem = false
    }
    
}

extension DetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let vc = viewController as? NotesViewController {
            contentView.stopPlay()
            vc.model.load()
        }
    }
    
}
