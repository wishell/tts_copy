//  
//  DetailsModel.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

final class DetailsModel: DetailsModelInput {
    
    weak var output: DetailsModelOutput!
    var coreDataHandler: CoreDataHandler!
    var note: Note?
    var isNewItem: Bool = true
    var text: String? {
        didSet {
            guard let text = text, var name = text.components(separatedBy: "\n").first else {return }
            name.append("...")
            if let item = note {
                item.name = item.name.isEmpty ? name : item.name
                item.content = text
                item.date = .now
            } else {
                note = Note(name: name, content: text)
            }
            note.flatMap { isNewItem ? add($0) : patch($0) }
        }
    }
    
    private func add(_ note: Note) {
        isNewItem = false
        coreDataHandler.save(note: note)
    }
    
    private func patch(_ note: Note) {
        coreDataHandler.patch(note: note)
    }
    
}
