//  
//  DetailsModelInput.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

protocol DetailsModelInput {
    var note: Note? { get set }
    var text: String? { get set }
    var isNewItem: Bool { get set }
}
