//  
//  DetailsView.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import AVFoundation

protocol DetailsViewInput: class {
    var saveText: ((String) -> Void)? { get set }
    func display (_ text: String?)
    func stopPlay()
}

final class DetailsView: UIView {
    
    var previousSelectedRange: NSRange!
    
    private var placeholderText = "Put you text here..."
    var speaker: Speaker!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    var saveText: ((String) -> Void)?
    
    @IBAction func saveAction(_ sender: Any) {
        textView.endEditing(true)
        guard let text = textView.text else { return }
        saveText?(text)
    }
    
    @IBAction func stopAction(_ sender: Any) {
        toggleSelection(actionBtn)
        speaker.stop()
        setInitialFontAttribute()
    }
    
    func save() {
        guard let text = textView.text else { return }
        saveText?(text)
    }
    
    @IBAction func buttonTap(_ sender: UIButton) {
        toggleSelection(sender)
        if sender.isSelected {
            speaker.speak(text: textView.text, with: 0.5)
        } else {
            speaker.pause()
        }
    }
    
    func stopPlay(){
        speaker.stop()
    }
    
    private func toggleSelection(_ object: UIControl) {
        object.isSelected = !object.isSelected
        stopBtn.isEnabled = object.isSelected
    }
    
    #if !TARGET_INTERFACE_BUILDER
    override func awakeFromNib() {
        super.awakeFromNib()
        
        speaker = Speaker()
        speaker.output = self
        speaker.delegate = speaker
        textView.placeholder = placeholderText
        textView.doneAction = { [weak textView] in textView?.endEditing(true) }
        setInitialFontAttribute()
        stopBtn.titleLabel?.font = .textStyle9
        stopBtn.titleLabel?.addCharacterSpacing(kernValue: 4.0)
    }
    #endif
    
    private func setInitialFontAttribute() {
        let rangeOfWholeText = NSRange(location: 0, length: textView.text.count)
        let attributedText = NSMutableAttributedString(string: textView.text)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.textStyle8, range: rangeOfWholeText)
        if #available(iOS 13.0, *) {
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.label, range: rangeOfWholeText)
        } 
        textView.textStorage.beginEditing()
        textView.textStorage.replaceCharacters(in: rangeOfWholeText, with: attributedText)
        textView.textStorage.endEditing()
    }
}

// MARK: - DetailsViewInput
extension DetailsView: DetailsViewInput {
    
    func display (_ text: String?) {
        textView.text = text
        textView.textViewDidChange(textView)
    }
    
}

extension DetailsView: SpeakerOutput {
    
    func didFinishRead() {
        setInitialFontAttribute()
        actionBtn.isSelected = false
        stopBtn.isEnabled = false
    }
    
    func willSpeakRangeOfSpeakString(_ rangeInTotalText: NSRange) {
        
        textView.selectedRange = rangeInTotalText
        guard let fontAttribute = textView.attributedText.attributes(at: rangeInTotalText.location, effectiveRange: nil)[.font] else { return }
        let attributedString = NSMutableAttributedString(string: textView.attributedText.attributedSubstring(from: rangeInTotalText).string)
        attributedString.addAttribute(.foregroundColor, value: UIColor.paleLilac, range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttribute(.font, value: fontAttribute, range: NSRange(location: 0, length: attributedString.length))
        
        textView.scrollRangeToVisible(rangeInTotalText)
        textView.textStorage.beginEditing()
        textView.textStorage.replaceCharacters(in: rangeInTotalText, with: attributedString)
        textView.textStorage.endEditing()
        previousSelectedRange = rangeInTotalText
    }
    
    func willSpeakRangeOfSpeakString(_ string: NSMutableAttributedString) {
        textView.attributedText = string
    }
    
}
