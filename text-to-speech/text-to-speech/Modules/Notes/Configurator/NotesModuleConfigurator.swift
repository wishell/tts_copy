//  
//  NotesModuleConfigurator.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

final class NotesModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? NotesViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: NotesViewController) {
        let model = NotesModel()
        model.output = viewController
        model.coreDataHandler = serviceLocator.getService(CoreDataHandler.self)
        viewController.model = model
    }
    
}
