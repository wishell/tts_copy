//  
//  NotesModuleInitializer.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

final class NotesModuleInitializer: NSObject {
    
    @IBOutlet weak var viewController: NotesViewController!
    
    override func awakeFromNib() {
        let configurator = NotesModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: viewController)
    }
    
}
