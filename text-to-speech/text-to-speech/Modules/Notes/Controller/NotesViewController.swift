//  
//  NotesViewController.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import GoogleMobileAds

extension StoryboardSegue {
    static var details = StoryboardSegue(id: "details")
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}

final class NotesViewController: UIViewController {
    
    var model: NotesModelInput!
    var tableView: UITableView! { return contentView.table }
    var dataSource: NotesDataSource!
    
    let notificationCenter = NotificationCenter.default
    weak var bannerView: GADBannerView!
    
    private let subService = SubscriptionService()
    lazy var contentView: NotesViewInput = { return view as! NotesViewInput }()
    lazy var emptyStateView: EmptyStateView = { return Bundle.main.loadNibNamed("EmptyStateView", owner: self, options: nil)![0] as! EmptyStateView }()
    
    private lazy var keyboardNotification: (CGRect) -> Void = {
            [weak self] rect in
            guard let `self` = self else { return }
            let offset = max(self.view.frame.height - self.view.safeAreaInsets.bottom - rect.minY + 50, 0)
            UIView.animate(withDuration: 0.25, animations: {
                self.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: offset, right: 0.0)
            })
        }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        model.load()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        render()
    }
    
    private func render() {
        if sharedGroupDefaults?.bool(forKey: "premium") ?? false {
            hideBanner()
        } else {
            showBanner()
            
            let time = sharedGroupDefaults?.double(forKey: "subs.screen") ?? 0
            let date = Date(timeIntervalSince1970: time)
            if time == 0 || date.timeIntervalSince(Date()) > 1 * 24 * 60 * 60 {
                perform(segue: .subscription)
            }
        }
    }
    
    private func configure() {
        notificationCenter.addObserver(self, selector: #selector(verifySubs), name: UIApplication.didBecomeActiveNotification, object: nil)
        notificationCenter.observeKeyboard(keyboardNotification)
        contentView.addAction = { [weak self] in
            self?.perform(segue: .details)
        }
        contentView.addBackground(emptyStateView)
        emptyStateView.action = contentView.addAction
        
        dataSource = NotesDataSource(tableView: tableView, model: model)
        dataSource.itemAction = { [weak self] note in
            self?.perform(segue: .details, with: note)
        }
        
        (tabBarController?.tabBar as? TabBarMiddleButtonProtocol).flatMap {
            $0.buttonAction = contentView.addAction
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.equal(.details), let destination = segue.destination as? DetailsViewControllerInput, let note = sender as? Note {
            destination.set(note)
        }
    }
    
    private func showBanner() {
        guard bannerView == nil else {
            return
        }
        let banner = GADBannerView(adSize: kGADAdSizeBanner)
        banner.adUnitID = "ca-app-pub-8199923004857210/2837424593"
        banner.rootViewController = self
        contentView.addBannerViewToView(banner)
        banner.load(GADRequest())
        bannerView = banner
    }
    
    private func hideBanner() {
        bannerView?.removeFromSuperview()
        bannerView = nil
    }
    
    @objc private func verifySubs() {
        if subService.needVerify() {
            subService.clear()
            subService.findActiveSub {
                [weak self] (_, _) in
                DispatchQueue.main.async {
                    self?.render()
                }
            }
        }
    }

}

// MARK: - NotesModelOutput
extension NotesViewController: NotesModelOutput {
    
    func modelDidLoad() {
        tableView.reloadData()
    }
    
}
