//  
//  NotesModel.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation
import CoreData
import UIKit

final class NotesModel: NotesModelInput {
    
    weak var output: NotesModelOutput!
    var coreDataHandler: CoreDataHandler!
    var files: [TtsItem]! = []
    
    func load() {
        guard let fileEntities: [TtsItem] = coreDataHandler.fetch() else { return }
        files = fileEntities
        output.modelDidLoad()
    }
    
    func delete(_ object: TtsItem) {
        coreDataHandler.delete(object: object)
        files.removeAll { $0.id == object.id }
        output.modelDidLoad()
    }

}
