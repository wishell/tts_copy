//  
//  NotesModelInput.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

protocol NotesModelInput {
    func load()
    func delete(_ object: TtsItem)
    var files: [TtsItem]! { get set }
}
