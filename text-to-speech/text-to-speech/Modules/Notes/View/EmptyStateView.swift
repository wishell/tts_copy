//
//  EmptyStateView.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/29/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

class EmptyStateView: UIView {

    var action: EmptyClosure?
    
    @IBAction func addDidTap(_ sender: Any) {
        action?()
    }

}
