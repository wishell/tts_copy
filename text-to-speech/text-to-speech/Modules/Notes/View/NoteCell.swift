//
//  NoteCell.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

class NoteCell: HighlightTableCell, ReusableViewType, NibLoadableViewType {
    
    @IBOutlet weak var title: UITextField!
    @IBOutlet weak var date: UILabel!
    var action: StringClosure?
    
    #if !TARGET_INTERFACE_BUILDER
    override func awakeFromNib() {
        super.awakeFromNib()
        
        title.delegate = self
        title.font = .textStyle8
        date.font = .textStyle2
    }
    #endif
    
    func display(note: Note) {
        title.text = note.name
        date.text = note.textDate
    }
    
    func beginEditing() {
        title.becomeFirstResponder()
    }
}

extension NoteCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        action?(textField.text ?? "")
    }
    
}
