//
//  NotesDataSource.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import CoreData

final class NotesDataSource: NSObject {
    
    let handler = serviceLocator.getService(CoreDataHandler.self)
    var files:[TtsItem] { return model.files ?? []}
    var model: NotesModelInput!
    weak var tableView: UITableView!
    var itemAction: NoteClosure?
    
    init(tableView: UITableView, model: NotesModelInput) {
        super.init()
        
        self.tableView = tableView
        self.model = model
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NoteCell.self)
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView()
    }
    
}

extension NotesDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if files.isEmpty {
            tableView.backgroundView?.isHidden = false
            tableView.separatorStyle = .none
        } else {
            tableView.backgroundView?.isHidden = true
            tableView.separatorStyle = .singleLine
        }
        return files.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NoteCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.action = {[unowned self] name in
            let file = self.files[indexPath.row]
            file.name = name
            self.handler.patch(note: Note(file: file))
            cell.isUserInteractionEnabled = false
            tableView.reloadRows(at: [indexPath], with: .none)
        }
        cell.display(note: Note(file: files[indexPath.row]))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = Note(file: files[indexPath.row])
        itemAction?(note)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension NotesDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .destructive, title:  "Delete", handler: { (_, _, success: (Bool) -> Void) in
            let item = self.model.files[indexPath.row]
            self.model.delete(item)
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
         let editAction = UIContextualAction(style: .normal, title: "Edit", handler: { (_, _, success: (Bool) -> Void) in
                let cell: NoteCell = tableView.dequeueReusableCell(for: indexPath)
                cell.title.isUserInteractionEnabled = true
                success(true)
                tableView.reloadRows(at: [indexPath], with: .none)
                cell.beginEditing()
               })
        
               editAction.backgroundColor = .vividBlue
        
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
}
