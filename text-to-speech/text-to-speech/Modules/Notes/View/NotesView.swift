//  
//  NotesView.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

protocol NotesViewInput: class {
    func addBannerViewToView (_ view: UIView)
    func addBackground (_ view: UIView) 
    var addAction: EmptyClosure? { get set }
    var table: UITableView! { get }
}

final class NotesView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func addDidTap(_ sender: Any) {
        addAction?()
    }
    
    var addAction: EmptyClosure?
    var table: UITableView! { return tableView }
    
}

// MARK: - NotesViewInput
extension NotesView: NotesViewInput {
    
    func addBannerViewToView (_ view: UIView) {
        tableView.tableHeaderView = view
    }
    
    func addBackground (_ view: UIView) {
        tableView.backgroundView = view
    }
    
}
