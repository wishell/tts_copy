//
//  SettingsTableViewController.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import MessageUI
import SafariServices

extension StoryboardSegue {
    static var subscription = StoryboardSegue(id: "subscription")
}

class SettingsTableViewController: UITableViewController, EmailFormType {
    
    enum CellIdentifiers: Int {
        case more_appplications
        case subscription
        case write_us
        case policy
        case terms
        
        var id: String {
            return "\(self)"
        }
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let identifier = CellIdentifiers(rawValue: indexPath.row) else {
            return
        }
        
        switch identifier {
        case .more_appplications:
            openAppStore()
        case .subscription:
            perform(segue: .subscription)
        case .write_us:
            writeUs()
        case .policy:
            openPolicy()
        case .terms:
            openTerms()
        }
        
        analytics.track(.settings_action, args: ["type": identifier.id])
    }
    
    private func openPolicy() {
        let vc = SFSafariViewController(url: URL(string: "https://vialyx.com/tts-privacy")!)
        present(vc, animated: true, completion: nil)
    }
    
    private func openTerms() {
        let vc = SFSafariViewController(url: URL(string: "https://vialyx.com/tts-terms")!)
        present(vc, animated: true, completion: nil)
    }
    
    private func openAppStore() {
        ApplicationRouter().open(appsURL)
    }
    
    private func writeUs() {
       send(message: feedbackMessage,
        to: [feedbackEmail],
        delegate: self)
    }
    
    private var appsURL: URL? {
        return URL(string: "itms-apps://itunes.apple.com/developer/maksim-vialykh/id576696336")
    }
    
    private var feedbackEmail: String {
        return "info@vialyx.com"
    }
    
    private var feedbackMessage: String {
        var message = "\n\n\n"
        message += "-----------"
        message += "\n"
        message += "Information below is of high importance."
        message += "\n\n"
        message += "App version - \(Bundle.version)(\(Bundle.build))"
        message += "\n"
        message += "System - \(UIDevice.current.model), \(Locale.current.identifier), \(UIDevice.current.systemVersion)"
        return message
    }

}

// MARK: - MFMailComposeViewControllerDelegate
extension SettingsTableViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
