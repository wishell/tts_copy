//
//  Speaker.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 5/27/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

protocol SpeakerOutput: class {
    func didFinishRead()
    func willSpeakRangeOfSpeakString(_ rangeInTotalText: NSRange)
    func willSpeakRangeOfSpeakString(_ string: NSMutableAttributedString)
    
}

protocol Speecheable {
    var delegate: AVSpeechSynthesizerDelegate! { get set }
    func speak (text: String, with rate: Float)
    func pause()
    func stop()
}

final class Speaker: NSObject, Speecheable {
    
    weak var output: SpeakerOutput?
    
    let synthesizer = AVSpeechSynthesizer()
    var totalUtterances: Int! = 0
    var currentUtterance: Int! = 0
    var totalTextLength: Int = 0
    var spokenTextLengths: Int = 0
    
    func speak (text: String, with rate: Float) {
        if !synthesizer.isSpeaking {
            try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .allowAirPlay])
            try? AVAudioSession.sharedInstance().setActive(true)
            let textParagraphs = text.components(separatedBy: "\n")

            totalUtterances = textParagraphs.count
            currentUtterance = 0
            totalTextLength = 0
            spokenTextLengths = 0
            
            for pieceOfText in textParagraphs {
                let speechUtterance = AVSpeechUtterance(string: pieceOfText)
                speechUtterance.rate = rate
            
                totalTextLength += pieceOfText.count
                synthesizer.speak(speechUtterance)
            }
        }
        else{
            synthesizer.continueSpeaking()
        }
    }
    
    func pause() {
        synthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
    }
    
    func stop(){
        synthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    var delegate: AVSpeechSynthesizerDelegate! {
        didSet {
            synthesizer.delegate = delegate
        }
    }
    
}

// MARK: - AVSpeechSynthesizerDelegate
extension Speaker: AVSpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        spokenTextLengths += utterance.speechString.count + 1
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        currentUtterance += 1
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let rangeInTotalText = NSRange(location: spokenTextLengths + characterRange.location, length: characterRange.length)
        output?.willSpeakRangeOfSpeakString(rangeInTotalText)
    }
    
}
