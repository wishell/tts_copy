//
//  SubscriptionViewController.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import StoreKit

extension SKProduct {
    
    var month: Int {
        guard let period = subscriptionPeriod else {
            return 0
        }
        switch period.unit {
        case .year:
            return period.numberOfUnits * 12
        case .month:
            return period.numberOfUnits
        default:
            return 0
        }
    }
    
}

class SubscriptionViewController: UIViewController {
    
    @IBOutlet var nameLs: [UILabel]!
    @IBOutlet var monthlyPriceLs: [UILabel]!
    @IBOutlet var totalPriceLs: [UILabel]!
    
    private let subService = SubscriptionService()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SwiftyStoreKit.retrieveProductsInfo(productIDs) {
            [weak self] (results) in
            DispatchQueue.main.async {
                self?.render(products: results.retrievedProducts)
            }
        }
        sharedGroupDefaults?.set(Date().timeIntervalSince1970, forKey: "subs.screen")
    }
    
    private func render(products: Set<SKProduct>) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "en_US")
        
        for product in products {
            var index = -1
            switch product.productIdentifier {
            case m1Sub:
                index = 0
            case m3Sub:
                index = 1
            case y1Sub:
                index = 2
            default:
                continue
            }
            nameLs[index].text = product.localizedTitle
            totalPriceLs[index].text = String(product.localizedPrice ?? "")
        
            guard let price = formatter.string(from: NSNumber(value: product.price.doubleValue / Double(product.month))) else {
                continue
            }

            monthlyPriceLs[index].text  = "\(price) / Month"
        }
    }
    
    private lazy var subHandler: (PurchaseResult) -> Void = {
        [weak self] res in
        switch res {
        case .success(let purchase):
            print("Purchased product ID: \(purchase.productId)")
            self?.finish(with: purchase.productId)
        case .error(let error):
            print("Purchase error: \(error)")
        }
    }
    
    private func finish(with productId: String) {
        subService.save(productId)
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func buy1mSub(_ sender: Any) {
        SwiftyStoreKit.purchaseProduct(m1Sub, completion: subHandler)
    }
    
    @IBAction func buy3mSub(_ sender: Any) {
        SwiftyStoreKit.purchaseProduct(m3Sub, completion: subHandler)
    }
    
    @IBAction func buy1ySub(_ sender: Any) {
        SwiftyStoreKit.purchaseProduct(y1Sub, completion: subHandler)
    }
    
    @IBAction func restoreSub(_ sender: Any) {
        subService.findActiveSub {
            [weak self] productId, _ in
            if productId != nil {
                DispatchQueue.main.async {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}
