//
//  NibLoadableViewType.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

protocol NibLoadableViewType: class {
    static var nibName: String { get }
    static var nib:     UINib { get }
}

extension NibLoadableViewType {
    
    static var nibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        let nib    = UINib(nibName: Self.nibName, bundle: bundle)
        return nib
    }
    
}
