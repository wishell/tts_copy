//
//  ReusableType.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 9/3/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

protocol ReusableViewType: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableViewType {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self)
    }
}
