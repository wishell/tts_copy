//
//  SegueType.swift
//  text-to-speech
//
//  Created by Anton Vishnevsky on 8/26/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

protocol SegueType {
    var id: String { get }
}

extension SegueType {
    
    var id: String { return String(describing: self) }
    
}
