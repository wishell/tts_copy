//
//  AppDelegate.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 5/12/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import SwiftyStoreKit
import KeychainAccess
import CoreData

let sharedGroupDefaults = UserDefaults(suiteName: "group.vialyx.text-to-speech")
let keychain = Keychain(service: "com.vialyx.text-to-speech")
let m1Sub = "com.vialyx.text_to_speech.1m"
let m3Sub = "com.vialyx.text_to_speech.3m"
let y1Sub = "com.vialyx.text_to_speech.1y"
let productIDs: Set<String> = [m1Sub, m3Sub, y1Sub]
let analytics = AnalyticService()
var serviceLocator: ServiceLocator!
var delegate: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    override init() {
        super.init()
        
        setupDependencies()
    }
    
    private func setupDependencies() {
        delegate = self
        let locator = LazyServiceLocator()
        serviceLocator = locator
        locator.addService(CoreDataHandler(delegate: delegate))
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        Style().apply()

        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break
                }
            }
        }
        
        return true
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TtsModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
