//
//  UIColor+Additions.swift
//  TTS
//
//  Generated on Zeplin. (9/28/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {

  @nonobjc class var softPink: UIColor {
    return UIColor(red: 247.0 / 255.0, green: 161.0 / 255.0, blue: 199.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var blueGrey: UIColor {
    return UIColor(red: 138.0 / 255.0, green: 138.0 / 255.0, blue: 143.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var cloudyBlue: UIColor {
    return UIColor(red: 183.0 / 255.0, green: 185.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var lightPeriwinkle: UIColor {
    return UIColor(red: 217.0 / 255.0, green: 218.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var softPinkTwo: UIColor {
    return UIColor(red: 1.0, green: 154.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var palePink: UIColor {
    return UIColor(red: 1.0, green: 196.0 / 255.0, blue: 217.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var lightPink: UIColor {
    return UIColor(red: 1.0, green: 228.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var white: UIColor {
    return UIColor(white: 1.0, alpha: 1.0)
  }

  @nonobjc class var black: UIColor {
    return UIColor(white: 28.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var paleLilac: UIColor {
    return UIColor(red: 222.0 / 255.0, green: 221.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var vividBlue: UIColor {
    return UIColor(red: 16.0 / 255.0, green: 47.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

  @nonobjc class var lightPeriwinkleTwo: UIColor {
    return UIColor(red: 179.0 / 255.0, green: 188.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

}