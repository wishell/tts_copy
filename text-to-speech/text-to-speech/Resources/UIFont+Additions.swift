//
//  UIFont+Additions.swift
//  TTS
//
//  Generated on Zeplin. (10/3/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIFont {

  class var textStyle: UIFont {
    return UIFont.systemFont(ofSize: 34.0, weight: .bold)
  }

  class var textStyle7: UIFont {
    return UIFont.systemFont(ofSize: 17.0, weight: .semibold)
  }

  class var textStyle6: UIFont {
    return UIFont.systemFont(ofSize: 17.0, weight: .semibold)
  }

  class var textStyle5: UIFont {
    return UIFont.systemFont(ofSize: 17.0, weight: .semibold)
  }

  class var textStyle10: UIFont {
    return UIFont.systemFont(ofSize: 17.0, weight: .regular)
  }

  class var textStyle8: UIFont {
    return UIFont.systemFont(ofSize: 17.0, weight: .regular)
  }

  class var textStyle9: UIFont {
    return UIFont.systemFont(ofSize: 15.0, weight: .semibold)
  }

  class var textStyle4: UIFont {
    return UIFont(name: "SFProText-Italic", size: 15.0)!
  }

  class var textStyle3: UIFont {
    return UIFont.systemFont(ofSize: 13.0, weight: .semibold)
  }

  class var textStyle2: UIFont {
    return UIFont.systemFont(ofSize: 13.0, weight: .regular)
  }

  class var textStyle11: UIFont {
    return UIFont.systemFont(ofSize: 10.0, weight: .regular)
  }

}