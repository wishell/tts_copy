//
//  ApplicationRouter.swift
//  breast-feeding
//
//  Created by Maxim Vialyx on 9/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

final class ApplicationRouter {
    
    private weak var    window:             UIWindow?
    private unowned let application:        UIApplication
    private unowned let notificationCenter: NotificationCenter

    init(window: UIWindow? = nil,
         application: UIApplication = UIApplication.shared,
         notificationCenter: NotificationCenter = NotificationCenter.default) {
        self.window = window
        self.application = application
        self.notificationCenter = notificationCenter
    }
    
}

extension ApplicationRouter: URLRouterType {
    
    func open(_ url: URL?) {
        guard let `url` = url else { return }
        application.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value) })
}
