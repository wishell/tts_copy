//
//  Bundle+InfoPlist.swift
//  breast-feeding
//
//  Created by Maxim Vialyx on 9/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

extension Bundle {

    static var appGroupIdentifier: String {
        return main.infoDictionary?["APP_GROUP"] as? String ?? ""
    }

    static var identifier: String {
        return main.bundleIdentifier ?? ""
    }

    static var displayName: String {
        return main.infoDictionary?["CFBundleDisplayName"] as? String ?? ""
    }

    static var name: String {
        return main.infoDictionary?[kCFBundleNameKey as String] as? String ?? ""
    }

    static var version: String {
        return main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }

    static var build: String {
        return main.infoDictionary?[kCFBundleVersionKey as String] as? String ?? ""
    }

}
