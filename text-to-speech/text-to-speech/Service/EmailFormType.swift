//
//  EmailFormType.swift
//  breast-feeding
//
//  Created by Maxim Vialyx on 9/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit
import MessageUI

protocol EmailFormType {
    var mailForms: [MailFormType] { get }
    var router:    URLRouterType! { get }
    func send(subject: String, message: String, to recepients: [String], delegate: MFMailComposeViewControllerDelegate)
}

extension EmailFormType where Self: UIViewController {

    var mailForms: [MailFormType] {
        return [GoogleMailForm(router: router),
                OutlookMailForm(router: router),
                YahooMailForm(router: router)]
    }

    var router: URLRouterType! {
        return ApplicationRouter()
    }

    func send(subject: String = "\(Bundle.displayName)", message: String = "\n\n\n\(Bundle.version)(\(Bundle.build))", to recepients: [String], delegate: MFMailComposeViewControllerDelegate) {
        if MFMailComposeViewController.canSendMail() {
            let controller = MFMailComposeViewController()
            controller.mailComposeDelegate = delegate
            controller.setSubject(subject)
            controller.setMessageBody(message, isHTML: false)
            controller.setToRecipients(recepients)
            present(controller, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            mailForms.forEach {
                form in
                alert.addAction(UIAlertAction(title: form.title, style: .default, handler: {
                    (_) in
                    form.send(subject: subject, message: message, to: recepients)
                }))
            }
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }

}

// MARK: - MailFormType
protocol MailFormType {
    var router: URLRouterType! { get set }
    var scheme: String { get }
    var action: String { get }
    var title:  String { get }
    func send(subject: String, message: String, to recepients: [String])
}

extension MailFormType {

    func send(subject: String, message: String, to recepients: [String]) {
        guard let to = recepients.joined(separator: ",").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
              let subject = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
              let body = message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
        let url = URL(string: "\(scheme)\(action)?to=\(to)&subject=\(subject)&body=\(body)")
        router.open(url)
    }

}

struct GoogleMailForm: MailFormType {

    var router: URLRouterType!
    var scheme: String { return "googlegmail://" }
    var action: String { return "co" }
    var title:  String { return "Google" }

}

struct OutlookMailForm: MailFormType {

    var router: URLRouterType!
    var scheme: String { return "ms-outlook://" }
    var action: String { return "compose" }
    var title:  String { return "Outlook" }

}

struct YahooMailForm: MailFormType {

    var router: URLRouterType!
    var scheme: String { return "ymail://mail/" }
    var action: String { return "compose" }
    var title:  String { return "Yahoo" }

}
