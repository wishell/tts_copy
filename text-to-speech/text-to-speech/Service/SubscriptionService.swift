//
//  SubscriptionService.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 9/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation
import SwiftyStoreKit

class SubscriptionService {
    
    func needVerify() -> Bool {
        let time = sharedGroupDefaults?.double(forKey: "subs.date") ?? 0
        let date = Date(timeIntervalSinceNow: time)
        return date.timeIntervalSince(Date()) > 1 * 24 * 60 * 60
    }
    
    func clear() {
        sharedGroupDefaults?.removeObject(forKey: "premium")
        sharedGroupDefaults?.removeObject(forKey: "subs.id")
    }
    
    func save(_ id: String) {
        sharedGroupDefaults?.set(true, forKey: "premium")
        sharedGroupDefaults?.set(id, forKey: "subs.id")
        sharedGroupDefaults?.set(Date().timeIntervalSinceNow, forKey: "subs.date")
    }
    
    func findActiveSub(_ compl: @escaping ((String?, Error?)-> Void)) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "a6a1c3b67c6345d8a7140f24407f1aff")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: true) {
            [weak self] result in
            switch result {
            case .success(let receipt):
                print("Verify receipt success: \(receipt)")
                let res = SwiftyStoreKit.verifySubscriptions(productIds: productIDs, inReceipt: receipt)
                switch res {
                case .purchased(_, let items):
                    for item in items {
                        if let expDate = item.subscriptionExpirationDate, expDate.timeIntervalSince(Date()) > 0 {
                            print("Restored product ID: \(item.productId)")
                            self?.save(item.productId)
                            compl(item.productId, nil)
                            break
                        }
                    }
                    break
                default:
                    compl(nil, nil)
                    break
                }
            case .error(let error):
                print("Verify receipt failed: \(error)")
                compl(nil, error)
            }
        }
    }
    
}
