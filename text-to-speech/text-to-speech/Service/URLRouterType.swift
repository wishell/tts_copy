//
//  URLRouterType.swift
//  breast-feeding
//
//  Created by Maxim Vialyx on 9/22/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import Foundation

protocol URLRouterType {
    func open(_ url: URL?)
}
