//
//  HighlightAnimatable.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 9/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

struct HighlightAnimatableSettings {
    let duration:       TimeInterval            = 0.5
    let delay:          TimeInterval            = 0.0
    let springDamping:  CGFloat                 = 1.0
    let springVelocity: CGFloat                 = 0.0
    let options:        UIView.AnimationOptions = [.allowUserInteraction]
    var transform:      CGAffineTransform       = .init(scaleX: 0.95, y: 0.95)
}

protocol HighlightAnimatable: class {
    var settings: HighlightAnimatableSettings { get }
    func lockAnimation()
    func unlockAnimation()
    func highlight(_ touched: Bool)
    func highlight(_ touched: Bool, completion: ((Bool) -> Void)?)
}

private struct AssociatedKeys {
    static var highlightAnimation = "VIV_highlightAnimation"
    static var initialTransform   = "VIV_initialTransform"
}

extension HighlightAnimatable where Self: UIView {

    private var animationAvailable: Bool {
        get { return objc_getAssociatedObject(self, &AssociatedKeys.highlightAnimation) as? Bool ?? true }
        set { objc_setAssociatedObject(self, &AssociatedKeys.highlightAnimation, newValue, .OBJC_ASSOCIATION_ASSIGN) }
    }

    private var initialTransform: CGAffineTransform {
        get { return ((objc_getAssociatedObject(self, &AssociatedKeys.initialTransform) as? NSValue)?.cgAffineTransformValue) ?? .identity }
        set { objc_setAssociatedObject(self, &AssociatedKeys.initialTransform, NSValue(cgAffineTransform: newValue), .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    var settings: HighlightAnimatableSettings { return HighlightAnimatableSettings() }

    func lockAnimation() {
        animationAvailable = false
        layer.removeAllAnimations()
    }

    func unlockAnimation() {
        animationAvailable = true
    }

    func highlight(_ touched: Bool) {
        highlight(touched) { (finish) in
            print("highlight touched: \(touched) finish: \(finish) transform: \(self.transform)")
        }
    }

    func highlight(_ touched: Bool, completion: ((Bool) -> Void)?) {
        guard animationAvailable else { return }
        if touched { initialTransform = self.transform }
        UIView.animate(withDuration: settings.duration,
                       delay: settings.delay,
                       usingSpringWithDamping: settings.springDamping,
                       initialSpringVelocity: settings.springVelocity,
                       options: settings.options,
                       animations: {
                           [weak self] in
                           self.flatMap { $0.transform = touched ? $0.settings.transform : $0.initialTransform }
                       },
                       completion: completion)
    }

}
