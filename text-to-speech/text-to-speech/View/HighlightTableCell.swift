//
//  HighlightTableCell.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 9/29/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

class HighlightTableCell: UITableViewCell, HighlightAnimatable {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        highlight(true)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        highlight(false)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        highlight(false)
    }

}
