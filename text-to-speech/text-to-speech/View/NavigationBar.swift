//
//  NavigationBar.swift
//  text-to-speech
//
//  Created by Maxim Vialyx on 9/28/19.
//  Copyright © 2019 Vialyx. All rights reserved.
//

import UIKit

class NavigationBar: UINavigationBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    private func configure() {
        
    }
    
}
